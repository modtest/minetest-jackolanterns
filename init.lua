local pumpkin_name_list = {}

local function register_pumpkin(pumpkin_name, light_level)
    local count = table.getn(pumpkin_name_list)

    if not pumpkin_name:find("_on") then
        pumpkin_name_list[count + 1] = pumpkin_name
    end

    minetest.register_node("jackolanterns:" .. pumpkin_name, {
        description = pumpkin_name:sub(1, 1):upper() .. pumpkin_name:sub(2, -1).. " Jackolantern",
        groups = {cracky = 2, choppy = 2},
        tiles = {
            "jackolanterns_pumpkin_top.png",
            "jackolanterns_pumpkin_top.png",
            "jackolanterns_pumpkin.png",
            "jackolanterns_pumpkin.png",
            "jackolanterns_pumpkin.png",
            "jackolanterns_" .. pumpkin_name .. ".png",
        },
        drawtype = "nodebox",
        paramtype = "light",
        paramtype2 = "facedir", 
        light_source = light_level,
        drop = "jackolanterns:pumpkin",
        node_box = {
            type = "fixed",
            fixed = {
                {-0.4375, -0.4375, -0.4375, 0.4375, 0.25, 0.4375},
                {-0.125, 0.3125, -0.0625, 0.0625, 0.4375, 0.125}, 
                {-0.375, -0.5, -0.375, 0.375, -0.4375, 0.375},
                {-0.375, 0.25, -0.375, 0.375, 0.3125, 0.375},
            }
        },
        on_rightclick = function (pos, node, clicker, itemstack)
            if node.name:find("_on") then
                minetest.set_node(pos, {name="jackolanterns:" .. pumpkin_name:sub(1, -4), param2 = minetest.get_node(pos).param2})
            else
                minetest.set_node(pos, {name="jackolanterns:" .. pumpkin_name .. "_on", param2 = minetest.get_node(pos).param2})
            end
        end
    })
end

local function register_jackolantern(jackolantern_name)
    register_pumpkin(jackolantern_name, 0)
    register_pumpkin(jackolantern_name .. "_on", 11)
end

minetest.register_craftitem("jackolanterns:carving_knife", {
    description = "Pumpkin Carving Knife",
    inventory_image = "jackolanterns_carving_knife.png",
    tool_capabilities = {
        full_punhc_interval = 1.2,
        max_drop_level = 0,
        damage_groups = {fleshy = 4},
    },

    on_use = function (itemstack, user, pointed_thing)
        local pos = minetest.get_pointed_thing_position(pointed_thing, above)

        if pos ~= nil then 
            local node = minetest.get_node(pos)

            if node.name:find("jackolanterns:") then
                for i=1, table.getn(pumpkin_name_list), 1 do 
                    if (pumpkin_name_list[i] == node.name:sub(15, -1)) then
                        if pumpkin_name_list[i + 1] ~= nil then
                            minetest.set_node(pos, {name="jackolanterns:" .. pumpkin_name_list[i + 1], param2 = minetest.get_node(pos).param2})
                        else
                            minetest.set_node(pos, {name="jackolanterns:" .. pumpkin_name_list[1], param2 = minetest.get_node(pos).param2})
                        end
                    end
                end
            end
        end
    end
})

minetest.register_node("jackolanterns:table", {
	description = "Table",
	groups = {choppy = 1},
	tiles = {
		"jackolanterns_table_top.png",
		"jackolanterns_table_top.png",
		"jackolanterns_table_side.png",
		"jackolanterns_table_side.png",
		"jackolanterns_table_side.png",
		"jackolanterns_table_side.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, 0.375, -0.5, 0.5, 0.5, 0.5},
			{-0.375, -0.5, -0.375, -0.1875, 0.375, -0.1875},
			{-0.375, -0.5, 0.1875, -0.1875, 0.375, 0.375},
			{0.1875, -0.5, 0.1875, 0.375, 0.375, 0.375},
			{0.1875, -0.5, -0.375, 0.375, 0.375, -0.1875},
			{-0.1875, 0.125, -0.3125, 0.1875, 0.25, -0.25},
			{-0.1875, 0.125, 0.25, 0.1875, 0.25, 0.3125},
		}
	}
})

minetest.register_craft({
    output = "jackolanterns:pumpkin",
    recipe = {
        {"", "default:leaves", ""},
        {"wool:orange", "default:torch", "wool:orange"},
        {"", "wool:orange", ""}
    }
})

minetest.register_craft({
    output = "jackolanterns:carving_knife",
    recipe = {
        {"", "default:steel_ingot", ""},
        {"default:steel_ingot", "", ""},
        {"", "default:stick", ""}
    }
})

minetest.register_craft({
    output = "jackolanterns:table 4",
    recipe = {
        {"default:tree", "default:wood", "default:tree"},
        {"default:tree", "default:fence_wood", "default:tree"},
        {"default:tree", "", "default:tree"},
    }
})

register_jackolantern("pumpkin")
register_jackolantern("me_irl")
register_jackolantern("bloody")
register_jackolantern("skelington")
register_jackolantern("traditional")
register_jackolantern("radioactive")
register_jackolantern("happy")
