![Screenshot](https://gitlab.com/sburris/minetest-jackolanterns/blob/master/screenshot.png)

# Description
This is a mod that adds a pumpkin which can be carved using a carving knife into jackolanterns with difference faces. Right clicking a pumpkin will make it light up. 

## License 
Code is MIT, Assets are CC0.